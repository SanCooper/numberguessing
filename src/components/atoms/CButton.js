import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';

export default class CButton extends Component {
  render() {
    const {style, title} = this.props;
    return (
      <View>
        <TouchableOpacity {...this.props} style={{...styles.button, ...style}}>
          <Text style={{fontWeight: 'bold', color: 'white', fontSize: 16}}>
            {title.toUpperCase()}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    padding: 10,
    backgroundColor: 'blue',
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 5,
}});
