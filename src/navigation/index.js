import * as React from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../screens/Home';
import TebakAngka from '../screens/TebakAngka';
import CobaAxios from '../screens/axios';
import Lifecycle from '../screens/lifecycle';
import Login from '../screens/Login';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Tebak Angka" component={TebakAngka} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Lifecycle" component={Lifecycle} />
        <Stack.Screen name="Axios" component={CobaAxios} />
        <Stack.Screen name="Home" component={Home} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
