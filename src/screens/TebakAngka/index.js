// 
import React, {Component} from 'react';
import {StyleSheet, View, Button, Text, TextInput} from 'react-native';
import CButton from '../../components/atoms/CButton';
import CText from '../../components/atoms/CText';

export default class MyProject extends Component {
  constructor() {
    super();
    this.state = {
      // This is our Default number value
      NumberHolder: 1,
      GuessNumber: 0,
    };
  }

  _GenerateRandomNumber = () => {
    const {GuessNumber} = this.state;
    var RandomNumber = Math.floor(Math.random() * 10) + 1;

    this.setState({
      NumberHolder: RandomNumber,
    });
    if (RandomNumber == GuessNumber) {
      alert(`Tebakanmu Benar!
Angka yg harus ditebak adalah : ${RandomNumber}`);
    } else {
      alert(`Tebakanmu Salah!
Angka yg harus ditebak adalah : ${RandomNumber}`);
    }
    
  };

  render() {
    const {GuessNumber} = this.state;
    return (
      <View style={styles.MainContainer}>
        <CText style={{color:"blue"}}>
            Tebak angka dari 1-10 
        </CText>
        <View style={styles.textInput}>
          <TextInput
            style={{fontSize: 16, borderWidth: 1, width: '20%', borderRadius: 20, textAlign: 'center'}}
            onChangeText={input => {
              this.setState({GuessNumber: input});
            }}
          />
        </View>
        <CButton style={{borderRadius: 10, width: '40%'}} title="Tebak" onPress={this._GenerateRandomNumber} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    justifyContent: 'space-around',
    marginVertical: 15,
    flexDirection: 'row',
  },
});
