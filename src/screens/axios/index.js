import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import axios, {Axios} from 'axios';
import CText from '../../components/atoms/CText';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    axios
      .get('https://jsonplaceholder.typicode.com/users')
      .then(res => this.setState({data: res.data}));
  }
  render() {
    const {data} = this.state;
    return (
      <View>
        {data.length > 0 &&
          data.map((value, i) => {
            return <Text key={i}>{value.name}</Text>;
          })}
      </View>
    );
  }
}

const styles = StyleSheet.create({

});
