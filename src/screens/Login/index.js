import axios from 'axios';
import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  Button,
  Alert,
} from 'react-native';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    axios
      .get('https://dummyjson.com/users')
      .then(res => this.setState({data: res.data.users}));
  }

  _submit() {
    const {data, username, password} = this.state;
    const {navigation} = this.props;
    data.filter(value => {
      if (value.username == username && value.password == password) {
        console.log('Berhasil');
        navigation.navigate('Axios');
      } else {
        Alert.alert('Login Gagal', 'Username/Password salah!');
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        {/* <Image style={styles.logo} source={logo} /> */}

        <View style={{width: '80%'}}>
          <TextInput
            style={styles.inputText}
            placeholder="Username"
            onChangeText={input => {
              this.setState({username: input});
            }}
          />
          <TextInput
            secureTextEntry
            style={styles.inputText}
            placeholder="Password"
            onChangeText={input => {
              this.setState({password: input});
            }}
          />
        </View>
        {/* <TouchableOpacity
          style={styles.loginBtn}
          onPress={() => {
            this._submit();
          }}>
          <Text style={{color: 'white', fontWeight: 'bold'}}>LOGIN</Text>
        </TouchableOpacity> */}
        <Button title="Login" onPress={() => this._submit()} />
        <View style={{flexDirection: 'row', width: '80%', paddingTop: 20}}>
          <View style={{flex: 1, height: 1, backgroundColor: 'white'}} />
          <Text style={{color: 'white'}}>Sign Up</Text>
          <View style={{flex: 1, height: 1, backgroundColor: 'white'}} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0d5f8a',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputText: {
    height: 50,
    color: 'white',
    width: '100%',
    backgroundColor: '#6ec3c1',
    borderRadius: 20,
    marginBottom: 20,
    justifyContent: 'center',
    padding: 10,
  },
  loginBtn: {
    width: '80%',
    backgroundColor: '#9dcc5f',
    borderRadius: 20,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  logo: {
    marginBottom: 40,
    width: 100,
    height: 100,
    borderRadius: 20,
  },
});
